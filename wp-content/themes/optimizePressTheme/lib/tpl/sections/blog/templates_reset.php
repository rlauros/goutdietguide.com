<div class="op-bsw-grey-panel-content op-bsw-grey-panel-no-sidebar cf">
    <label for="op_favicon" class="form-title"><?php _e('Reset Content Templates',OP_SN) ?></label>
    <p class="op-micro-copy"><?php _e('Here you can remove all preinstalled content templates and reinstall it again. Use this only if you experience problems!',OP_SN) ?></p>
    <input type ="checkbox" name="op[sections][content_templates_reset]" id="op_content_templates_reset" /> <?php echo __('Yes, Reset Content Templates', OP_SN);?>
    <div class="clear"></div>
</div>