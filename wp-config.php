<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'goutdiet_db');

/** MySQL database username */
define('DB_USER', 'goutdiet_db');

/** MySQL database password */
define('DB_PASSWORD', 'admin1234');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '-t@ 3BGqm!MvAtx4b)Hl>-Z^95UVj:o$30P~o>/n +w1i$Sl()1]hBAl+c4tZ+]Q');
define('SECURE_AUTH_KEY',  'l-/[@gYG$rn)][+e?HlVjx22Ox^@)?aCP;fqBi.8`*C;Pq)H!QQqTU7_D UqI|le');
define('LOGGED_IN_KEY',    'mmUt^J4B$p<-Vl>rU3JFW/W%1_]-zB*]@;@81-{/:C7R<~9B20??2MpO:Yh{89nL');
define('NONCE_KEY',        '4xJWpaRwkW.8;$?N5F_|qxRKmFcOWU/+0pmgNvmlCKzwcTB8BN>5c(dErv+n{f9j');
define('AUTH_SALT',        '92Jx-vP9_uxK#s+2+`mbA$ILjUvM-R/fn,-[95au^@73,M*6e }9Z-?fQ,<4<<&{');
define('SECURE_AUTH_SALT', 'YZ$Ez-<CRac/n(DN*x=qr`JT@sMJ7;/JY8D^!}%UbibYrRLaR+W6#|C%m UiXhG-');
define('LOGGED_IN_SALT',   '-)J1+Jlf0,>dU+(YJB(tS|,<kO;umqdf;mB%86ws#Hk3-07&|rQ`8k+V/psv~ LS');
define('NONCE_SALT',       'YM-6$y$uehR2Od-o{`.QK>7i;ob#Z[5Y3iD<DS2E#T5vs11v?^`@LqCla(NZbEo,');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * WordPress Localized Language, defaults to English.
 *
 * Change this to localize WordPress. A corresponding MO file for the chosen
 * language must be installed to wp-content/languages. For example, install
 * de_DE.mo to wp-content/languages and set WPLANG to 'de_DE' to enable German
 * language support.
 */
define('WPLANG', '');

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
